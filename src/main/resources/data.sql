-- Adding some assets
INSERT INTO asset(isin, name) VALUES ('FR0011716265', 'CROSSJECT');
INSERT INTO asset(isin, name) VALUES ('US5949181045', 'MICROSOFT');
INSERT INTO asset(isin, name) VALUES ('NL0000235190', 'AIRBUS');
INSERT INTO asset(isin, name) VALUES ('GB00BN7CG237', 'ASTON MARTIN LGD');

-- Adding a portfolio
INSERT INTO portfolio(id, name, reference_currency) VALUES ('7b67169b-2f83-40ed-af09-fb81442787a8', 'Default portfolio', 'EUR');
INSERT INTO portfolio_position(portfolio_id, isin, quantity) VALUES ('7b67169b-2f83-40ed-af09-fb81442787a8', 'FR0011716265', 1000);
INSERT INTO portfolio_position(portfolio_id, isin, quantity) VALUES ('7b67169b-2f83-40ed-af09-fb81442787a8', 'US5949181045', 150);
INSERT INTO portfolio_position(portfolio_id, isin, quantity) VALUES ('7b67169b-2f83-40ed-af09-fb81442787a8', 'NL0000235190', 200);
