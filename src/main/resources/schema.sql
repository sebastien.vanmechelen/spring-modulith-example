drop table if exists asset cascade;
drop table if exists portfolio cascade;
drop table if exists portfolio_position cascade;
drop table if exists asset_recommendation cascade;

create table asset (
    isin varchar(12) not null,
    name varchar(255) not null,
    primary key (isin)
);

create table portfolio (
    id varchar(36) not null,
    name varchar(255) not null,
    reference_currency varchar(255),
    primary key (id)
);

create table portfolio_position (
    isin varchar(12) not null,
    quantity numeric(38,2),
    portfolio_id varchar(255) not null,
    primary key (isin)
);

alter table if exists portfolio_position add constraint fk_portfolio_position_to_portfolio foreign key (portfolio_id) references portfolio;

create table asset_recommendation (
    isin varchar(12) not null,
    recommendation_type varchar(16) not null,
    primary key (isin)
);

alter table if exists asset_recommendation add constraint fk_asset_recommendation_to_asset foreign key (isin) references asset;