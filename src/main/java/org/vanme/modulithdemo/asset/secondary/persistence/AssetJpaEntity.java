package org.vanme.modulithdemo.asset.secondary.persistence;

import jakarta.annotation.Nonnull;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.springframework.data.domain.AbstractAggregateRoot;
import org.vanme.modulithdemo.asset.Asset;
import org.vanme.modulithdemo.asset.Isin;

@Entity(name = "asset")
public class AssetJpaEntity extends AbstractAggregateRoot<AssetJpaEntity> {

    @Id
    @Column(name = "isin", length = 12)
    private String isin;

    @Nonnull
    @Column(name = "name")
    private String name;

    public AssetJpaEntity() {
    }

    public AssetJpaEntity(Asset asset) {
        this();
        setIsin(asset.getIsin().value())
                .setName(asset.getName());
    }

    public Asset toAsset() {
        return new Asset(new Isin(isin), name);
    }

    public String getIsin() {
        return isin;
    }

    public AssetJpaEntity setIsin(String isin) {
        this.isin = isin;
        return this;
    }

    public String getName() {
        return name;
    }

    public AssetJpaEntity setName(String name) {
        this.name = name;
        return this;
    }
}
