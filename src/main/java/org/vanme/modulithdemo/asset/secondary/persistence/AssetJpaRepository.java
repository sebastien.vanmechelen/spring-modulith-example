package org.vanme.modulithdemo.asset.secondary.persistence;

import org.jmolecules.ddd.annotation.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface AssetJpaRepository extends JpaRepository<AssetJpaEntity, String> {

}
