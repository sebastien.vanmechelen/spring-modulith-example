package org.vanme.modulithdemo.asset.secondary;

import org.springframework.stereotype.Component;
import org.vanme.modulithdemo.asset.Asset;
import org.vanme.modulithdemo.asset.AssetPort;
import org.vanme.modulithdemo.asset.Isin;
import org.vanme.modulithdemo.asset.secondary.persistence.AssetJpaEntity;
import org.vanme.modulithdemo.asset.secondary.persistence.AssetJpaRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
class AssetAdapter implements AssetPort {

    private final AssetJpaRepository assetJpaRepository;

    public AssetAdapter(AssetJpaRepository assetJpaRepository) {
        this.assetJpaRepository = assetJpaRepository;
    }

    @Override
    public Optional<Asset> findAsset(Isin isin) {
        return assetJpaRepository.findById(isin.value())
                .map(AssetJpaEntity::toAsset);
    }

    @Override
    public List<Asset> findAllAssets() {
        return assetJpaRepository.findAll()
                .stream()
                .map(AssetJpaEntity::toAsset)
                .sorted(Comparator.comparing(Asset::getName))
                .collect(Collectors.toList());
    }
}
