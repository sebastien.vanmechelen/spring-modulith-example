package org.vanme.modulithdemo.asset.primary.restcontroller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.vanme.modulithdemo.asset.AssetPort;

import java.util.List;

@RestController
@RequestMapping(path = "/assets")
class AssetController {

    private final AssetPort assetPort;

    public AssetController(AssetPort assetPort) {
        this.assetPort = assetPort;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<AssetDTO> getAllAssets() {
        return assetPort.findAllAssets()
                .stream()
                .map(AssetDTO::new)
                .toList();
    }
}
