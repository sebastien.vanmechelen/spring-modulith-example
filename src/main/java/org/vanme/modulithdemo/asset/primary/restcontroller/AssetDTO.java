package org.vanme.modulithdemo.asset.primary.restcontroller;

import org.vanme.modulithdemo.asset.Asset;

class AssetDTO {

    private final String isin;
    private final String name;

    public AssetDTO(Asset asset) {
        this.isin = asset.getIsin().value();
        this.name = asset.getName();
    }

    public String getIsin() {
        return isin;
    }

    public String getName() {
        return name;
    }
}
