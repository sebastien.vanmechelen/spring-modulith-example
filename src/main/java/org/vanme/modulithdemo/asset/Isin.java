package org.vanme.modulithdemo.asset;

import org.jmolecules.ddd.annotation.ValueObject;
import org.springframework.util.Assert;

@ValueObject
public record Isin(String value) {

    public Isin {
        Assert.isTrue(value.length() == 12, () -> "Value '%s' is not a valid ISIN".formatted(value));
    }

    @Override
    public String toString() {
        return value;
    }
}
