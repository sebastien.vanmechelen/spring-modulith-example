package org.vanme.modulithdemo.asset;

import org.jmolecules.architecture.hexagonal.SecondaryPort;

import java.util.List;
import java.util.Optional;

@SecondaryPort
public interface AssetPort {

    Optional<Asset> findAsset(Isin isin);

    List<Asset> findAllAssets();

}
