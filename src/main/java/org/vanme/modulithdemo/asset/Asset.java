package org.vanme.modulithdemo.asset;

import org.jmolecules.ddd.annotation.AggregateRoot;
import org.jmolecules.ddd.annotation.Identity;

// TODO - We can make a status to ask to close / expire this asset that makes remove any existing recommendation

@AggregateRoot
public class Asset {

    @Identity
    private final Isin isin;
    private final String name;

    public Asset(Isin isin, String name) {
        this.isin = isin;
        this.name = name;
    }

    public Isin getIsin() {
        return isin;
    }

    public String getName() {
        return name;
    }

}
