package org.vanme.modulithdemo.portfolio;

import org.jmolecules.ddd.annotation.ValueObject;

@ValueObject
public record PortfolioId(String value) {

    @Override
    public String toString() {
        return value;
    }
}
