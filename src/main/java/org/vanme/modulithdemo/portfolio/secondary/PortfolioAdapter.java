package org.vanme.modulithdemo.portfolio.secondary;

import org.springframework.stereotype.Component;
import org.vanme.modulithdemo.asset.Isin;
import org.vanme.modulithdemo.portfolio.Portfolio;
import org.vanme.modulithdemo.portfolio.PortfolioPort;
import org.vanme.modulithdemo.portfolio.secondary.persistence.PortfolioJpaEntity;
import org.vanme.modulithdemo.portfolio.secondary.persistence.PortfolioJpaRepository;

import java.util.List;

@Component
class PortfolioAdapter implements PortfolioPort {

    private final PortfolioJpaRepository portfolioJpaRepository;

    public PortfolioAdapter(PortfolioJpaRepository portfolioJpaRepository) {
        this.portfolioJpaRepository = portfolioJpaRepository;
    }

    @Override
    public List<Portfolio> findPortfoliosWith(Isin isin) {
        return portfolioJpaRepository.findByPositions_Isin(isin.value())
                .stream()
                .map(PortfolioJpaEntity::toPortfolio)
                .toList();
    }

    @Override
    public List<Portfolio> findAllPortfolios() {
        return portfolioJpaRepository.findAll()
                .stream()
                .map(PortfolioJpaEntity::toPortfolio)
                .toList();
    }

    @Override
    public void update(Portfolio portfolio) {
        var portfolioEntity = portfolioJpaRepository.getReferenceById(portfolio.getId().value());
        portfolioJpaRepository.save(portfolioEntity.update(portfolio));
    }
}
