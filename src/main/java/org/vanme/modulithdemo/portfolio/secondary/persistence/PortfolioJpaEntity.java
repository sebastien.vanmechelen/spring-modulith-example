package org.vanme.modulithdemo.portfolio.secondary.persistence;

import jakarta.persistence.*;
import org.springframework.data.domain.AbstractAggregateRoot;
import org.springframework.data.util.Pair;
import org.vanme.modulithdemo.asset.Isin;
import org.vanme.modulithdemo.portfolio.Portfolio;
import org.vanme.modulithdemo.portfolio.PortfolioComposition;
import org.vanme.modulithdemo.portfolio.PortfolioId;
import org.vanme.modulithdemo.shared.Currency;

import java.math.BigDecimal;
import java.util.Set;
import java.util.stream.Collectors;

@Entity(name = "portfolio")
public class PortfolioJpaEntity extends AbstractAggregateRoot<PortfolioJpaEntity> {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "reference_currency")
    private String referenceCurrency;

    @OneToMany(mappedBy="portfolio", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<PortfolioPositionJpaEntity> positions;

    public PortfolioJpaEntity() {
    }

    public Portfolio toPortfolio() {
        var quantityByIsin = getPositions().stream()
                .map(position -> Pair.of(new Isin(position.getIsin()), position.getQuantity()))
                .collect(Collectors.toMap(Pair::getFirst, Pair::getSecond));

        return new Portfolio(new PortfolioId(id), name, new Currency(referenceCurrency), new PortfolioComposition(quantityByIsin));
    }

    public PortfolioJpaEntity update(Portfolio portfolio) {
        name = portfolio.getName();
        referenceCurrency = portfolio.getReferenceCurrency().isoCode();
        positions.removeIf(position -> portfolio.noPositionOn(new Isin(position.getIsin())));
        portfolio.forEachPosition(this::createOrUpdatePosition);
        portfolio.getEvents().forEach(this::registerEvent);
        return this;
    }

    public String getId() {
        return id;
    }

    public PortfolioJpaEntity setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public PortfolioJpaEntity setName(String name) {
        this.name = name;
        return this;
    }

    public String getReferenceCurrency() {
        return referenceCurrency;
    }

    public PortfolioJpaEntity setReferenceCurrency(String referenceCurrency) {
        this.referenceCurrency = referenceCurrency;
        return this;
    }

    public Set<PortfolioPositionJpaEntity> getPositions() {
        return positions;
    }

    private void createOrUpdatePosition(Isin isin, BigDecimal quantity) {
        positions.stream()
                .filter(position -> position.getIsin().equals(isin.value()))
                .findFirst()
                .ifPresentOrElse(
                        position -> position.setQuantity(quantity),
                        () -> positions.add(new PortfolioPositionJpaEntity(this, isin.value(), quantity)));
    }
}
