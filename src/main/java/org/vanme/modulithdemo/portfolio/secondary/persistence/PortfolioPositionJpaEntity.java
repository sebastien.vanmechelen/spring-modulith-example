package org.vanme.modulithdemo.portfolio.secondary.persistence;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import java.math.BigDecimal;

@Entity(name = "portfolio_position")
public class PortfolioPositionJpaEntity {

    @ManyToOne
    @JoinColumn(name="portfolio_id", updatable = false, nullable = false)
    private PortfolioJpaEntity portfolio;

    @Id
    @Column(name = "isin", length = 12)
    private String isin;

    @Column(name = "quantity")
    private BigDecimal quantity;

    public PortfolioPositionJpaEntity() {
    }

    public PortfolioPositionJpaEntity(PortfolioJpaEntity portfolio, String isin, BigDecimal quantity) {
        this();
        this.portfolio = portfolio;
        this.isin = isin;
        this.quantity = quantity;
    }

    public PortfolioJpaEntity getPortfolio() {
        return portfolio;
    }

    public PortfolioPositionJpaEntity setPortfolio(PortfolioJpaEntity portfolio) {
        this.portfolio = portfolio;
        return this;
    }

    public String getIsin() {
        return isin;
    }

    public PortfolioPositionJpaEntity setIsin(String isin) {
        this.isin = isin;
        return this;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public PortfolioPositionJpaEntity setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
        return this;
    }
}
