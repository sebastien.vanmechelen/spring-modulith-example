package org.vanme.modulithdemo.portfolio.secondary.persistence;

import org.jmolecules.ddd.annotation.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface PortfolioJpaRepository extends JpaRepository<PortfolioJpaEntity, String> {

    List<PortfolioJpaEntity> findByPositions_Isin(String isin);

}
