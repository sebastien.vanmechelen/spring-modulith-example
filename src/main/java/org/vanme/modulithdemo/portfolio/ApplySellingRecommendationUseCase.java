package org.vanme.modulithdemo.portfolio;

import org.jmolecules.architecture.hexagonal.PrimaryPort;
import org.vanme.modulithdemo.asset.Isin;

@PrimaryPort
public interface ApplySellingRecommendationUseCase {

    void execute(Isin isin);

}
