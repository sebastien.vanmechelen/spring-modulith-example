package org.vanme.modulithdemo.portfolio;

import org.jmolecules.ddd.annotation.AggregateRoot;
import org.jmolecules.ddd.annotation.Identity;
import org.vanme.modulithdemo.asset.Isin;
import org.vanme.modulithdemo.shared.Currency;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

@AggregateRoot
public class Portfolio {

    @Identity
    private final PortfolioId id;
    private final String name;
    private final Currency referenceCurrency;
    private PortfolioComposition portfolioComposition;
    private final List<Object> events;

    public Portfolio(PortfolioId id,
                     String name,
                     Currency referenceCurrency,
                     PortfolioComposition portfolioComposition) {

        this.id = id;
        this.name = name;
        this.referenceCurrency = referenceCurrency;
        this.portfolioComposition = portfolioComposition;
        this.events = new ArrayList<>();
    }

    public PortfolioId getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Currency getReferenceCurrency() {
        return referenceCurrency;
    }

    public Portfolio sellAll(Isin isin) {
        events.add(new PositionChangedEvent(id, isin, getQuantity(isin).negate()));
        portfolioComposition = portfolioComposition.sellAll(isin);
        return this;
    }

    public Portfolio buy(Isin isin, BigDecimal quantity) {
        events.add(new PositionChangedEvent(id, isin, quantity));
        portfolioComposition = portfolioComposition.buy(isin, quantity);
        return this;
    }

    public boolean noPositionOn(Isin isin) {
        return !hasPositionOn(isin);
    }

    public boolean hasPositionOn(Isin isin) {
        return getQuantity(isin).signum() != 0;
    }

    public BigDecimal getQuantity(Isin isin) {
        return portfolioComposition.quantityByIsin()
                .getOrDefault(isin, BigDecimal.ZERO);
    }

    public void forEachPosition(BiConsumer<Isin, BigDecimal> consumer) {
        portfolioComposition.quantityByIsin().forEach(consumer);
    }

    public List<Object> getEvents() {
        return List.copyOf(events);
    }
}
