package org.vanme.modulithdemo.portfolio;

import org.jmolecules.event.annotation.DomainEvent;
import org.vanme.modulithdemo.asset.Isin;

import java.math.BigDecimal;

@DomainEvent
public record PositionChangedEvent(PortfolioId portfolioId, Isin isin, BigDecimal variation) {

}
