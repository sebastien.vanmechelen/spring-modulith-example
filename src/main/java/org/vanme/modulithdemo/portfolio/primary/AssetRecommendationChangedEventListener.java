package org.vanme.modulithdemo.portfolio.primary;

import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;
import org.vanme.modulithdemo.portfolio.ApplyBuyingRecommendationUseCase;
import org.vanme.modulithdemo.portfolio.ApplySellingRecommendationUseCase;
import org.vanme.modulithdemo.recommendation.AssetRecommendationChangedEvent;

@Component
class AssetRecommendationChangedEventListener {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(AssetRecommendationChangedEventListener.class);

    public final ApplySellingRecommendationUseCase applySellingRecommendationUseCase;
    public final ApplyBuyingRecommendationUseCase applyBuyingRecommendationUseCase;

    public AssetRecommendationChangedEventListener(ApplySellingRecommendationUseCase applySellingRecommendationUseCase,
                                                   ApplyBuyingRecommendationUseCase applyBuyingRecommendationUseCase) {

        this.applySellingRecommendationUseCase = applySellingRecommendationUseCase;
        this.applyBuyingRecommendationUseCase = applyBuyingRecommendationUseCase;
    }

    @TransactionalEventListener(condition = "#event.isSellingRecommendation()")
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    @Async
    public void onSellingRecommendation(AssetRecommendationChangedEvent event) {
        LOGGER.info("Received selling recommendation {}", event);
        applySellingRecommendationUseCase.execute(event.isin());
    }

    @TransactionalEventListener(condition = "#event.isBuyingRecommendation()")
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    @Async
    public void onBuyingRecommendation(AssetRecommendationChangedEvent event) {
        LOGGER.info("Received buying recommendation {}", event);
        applyBuyingRecommendationUseCase.execute(event.isin());
    }
}
