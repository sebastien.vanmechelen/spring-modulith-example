package org.vanme.modulithdemo.portfolio.internal;

import org.jmolecules.architecture.hexagonal.PrimaryAdapter;
import org.springframework.stereotype.Service;
import org.vanme.modulithdemo.asset.Isin;
import org.vanme.modulithdemo.portfolio.ApplyBuyingRecommendationUseCase;
import org.vanme.modulithdemo.portfolio.PortfolioPort;

import java.math.BigDecimal;

@PrimaryAdapter
@Service
class ApplyBuyingRecommendationService implements ApplyBuyingRecommendationUseCase {

    private static final BigDecimal DEFAULT_QTY_TO_BUY = BigDecimal.TEN;

    private final PortfolioPort portfolioPort;

    public ApplyBuyingRecommendationService(PortfolioPort portfolioPort) {
        this.portfolioPort = portfolioPort;
    }

    @Override
    public void execute(Isin isin) {
        portfolioPort.findAllPortfolios()
                .stream()
                .map(portfolio -> portfolio.buy(isin, DEFAULT_QTY_TO_BUY))
                .forEach(portfolioPort::update);
    }
}
