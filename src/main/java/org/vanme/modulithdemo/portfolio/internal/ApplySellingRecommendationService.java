package org.vanme.modulithdemo.portfolio.internal;

import org.jmolecules.architecture.hexagonal.PrimaryAdapter;
import org.springframework.stereotype.Service;
import org.vanme.modulithdemo.asset.Isin;
import org.vanme.modulithdemo.portfolio.ApplySellingRecommendationUseCase;
import org.vanme.modulithdemo.portfolio.PortfolioPort;

@PrimaryAdapter
@Service
class ApplySellingRecommendationService implements ApplySellingRecommendationUseCase {

    private final PortfolioPort portfolioPort;

    public ApplySellingRecommendationService(PortfolioPort portfolioPort) {
        this.portfolioPort = portfolioPort;
    }

    @Override
    public void execute(Isin isin) {
        portfolioPort.findPortfoliosWith(isin)
                .stream()
                .map(portfolio -> portfolio.sellAll(isin))
                .forEach(portfolioPort::update);
    }
}
