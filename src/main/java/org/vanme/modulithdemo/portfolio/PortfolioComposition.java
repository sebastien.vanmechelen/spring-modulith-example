package org.vanme.modulithdemo.portfolio;

import org.jmolecules.ddd.annotation.ValueObject;
import org.vanme.modulithdemo.asset.Isin;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@ValueObject
public record PortfolioComposition(Map<Isin, BigDecimal> quantityByIsin) {

    public PortfolioComposition {
        quantityByIsin = Map.copyOf(quantityByIsin);
    }

    public PortfolioComposition sellAll(Isin isin) {
        var newComposition = new HashMap<>(quantityByIsin);
        newComposition.remove(isin);
        return new PortfolioComposition(newComposition);
    }

    public PortfolioComposition buy(Isin isin, BigDecimal quantityToBuy) {
        var newComposition = new HashMap<>(quantityByIsin);
        var currentQuantity = newComposition.getOrDefault(isin, BigDecimal.ZERO);
        newComposition.put(isin, currentQuantity.add(quantityToBuy));
        return new PortfolioComposition(newComposition);
    }
}
