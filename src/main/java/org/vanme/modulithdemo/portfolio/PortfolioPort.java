package org.vanme.modulithdemo.portfolio;

import org.vanme.modulithdemo.asset.Isin;

import java.util.List;

public interface PortfolioPort {

    List<Portfolio> findPortfoliosWith(Isin isin);

    List<Portfolio> findAllPortfolios();

    void update(Portfolio portfolio);

}
