package org.vanme.modulithdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableAsync
@EnableTransactionManagement
public class ModulithDemoApplication {

	private static final Logger logger = LoggerFactory
			.getLogger(ModulithDemoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ModulithDemoApplication.class, args);
	}

}
