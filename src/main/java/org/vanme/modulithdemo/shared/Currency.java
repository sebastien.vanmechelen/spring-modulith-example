package org.vanme.modulithdemo.shared;

import org.jmolecules.ddd.annotation.ValueObject;

@ValueObject
public record Currency(String isoCode) {

    public Currency {
        isoCode = isoCode.trim().toUpperCase();
    }

    @Override
    public String toString() {
        return isoCode;
    }
}
