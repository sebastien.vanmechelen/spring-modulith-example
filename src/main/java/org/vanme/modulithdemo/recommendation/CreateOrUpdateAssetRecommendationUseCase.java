package org.vanme.modulithdemo.recommendation;

import org.jmolecules.architecture.hexagonal.PrimaryPort;
import org.vanme.modulithdemo.asset.Isin;

@PrimaryPort
public interface CreateOrUpdateAssetRecommendationUseCase {

    void execute(Isin isin, RecommendationType recommendationType);

}
