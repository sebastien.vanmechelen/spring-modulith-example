package org.vanme.modulithdemo.recommendation;

import org.jmolecules.ddd.annotation.ValueObject;

@ValueObject
public enum RecommendationType {
    SELL,
    BUY,
    NONE
}
