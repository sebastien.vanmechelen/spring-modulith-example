package org.vanme.modulithdemo.recommendation.secondary.persistence;

import org.jmolecules.ddd.annotation.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface AssetRecommendationJpaRepository extends JpaRepository<AssetRecommendationJpaEntity, String> {

}
