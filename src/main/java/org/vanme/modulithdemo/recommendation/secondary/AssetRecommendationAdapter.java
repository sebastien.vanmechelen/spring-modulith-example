package org.vanme.modulithdemo.recommendation.secondary;

import org.springframework.stereotype.Component;
import org.vanme.modulithdemo.asset.Isin;
import org.vanme.modulithdemo.recommendation.AssetRecommendation;
import org.vanme.modulithdemo.recommendation.AssetRecommendationPort;
import org.vanme.modulithdemo.recommendation.secondary.persistence.AssetRecommendationJpaEntity;
import org.vanme.modulithdemo.recommendation.secondary.persistence.AssetRecommendationJpaRepository;

import java.util.Collection;

@Component
class AssetRecommendationAdapter implements AssetRecommendationPort {

    private final AssetRecommendationJpaRepository assetRecommendationJpaRepository;

    public AssetRecommendationAdapter(AssetRecommendationJpaRepository assetRecommendationJpaRepository) {
        this.assetRecommendationJpaRepository = assetRecommendationJpaRepository;
    }

    @Override
    public Collection<AssetRecommendation> findAll() {
        return assetRecommendationJpaRepository.findAll()
                .stream()
                .map(AssetRecommendationJpaEntity::toAssetRecommendation)
                .toList();
    }

    @Override
    public AssetRecommendation getByIsin(Isin isin) {
        return assetRecommendationJpaRepository.findById(isin.value())
                .map(AssetRecommendationJpaEntity::toAssetRecommendation)
                .orElseGet(() -> new AssetRecommendation(isin));
    }

    @Override
    public void save(AssetRecommendation assetRecommendation) {
        assetRecommendationJpaRepository.save(new AssetRecommendationJpaEntity(assetRecommendation));
    }
}
