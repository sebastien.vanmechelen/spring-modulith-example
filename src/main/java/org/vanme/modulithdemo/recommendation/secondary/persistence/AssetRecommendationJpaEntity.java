package org.vanme.modulithdemo.recommendation.secondary.persistence;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import org.springframework.data.domain.AbstractAggregateRoot;
import org.vanme.modulithdemo.asset.Isin;
import org.vanme.modulithdemo.recommendation.AssetRecommendation;
import org.vanme.modulithdemo.recommendation.RecommendationType;

@Entity(name = "asset_recommendation")
public class AssetRecommendationJpaEntity extends AbstractAggregateRoot<AssetRecommendationJpaEntity> {

    @Id
    @Column(name = "isin", length = 12)
    private String isin;

    @Column(name = "recommendation_type")
    private String recommendationType;

    public AssetRecommendationJpaEntity() {
    }

    public AssetRecommendationJpaEntity(AssetRecommendation assetRecommendation) {
        this();
        setIsin(assetRecommendation.getIsin().value());
        setRecommendationType(assetRecommendation.getRecommendationType().name());

        assetRecommendation.getEvents()
                .forEach(this::registerEvent);
    }

    public AssetRecommendation toAssetRecommendation() {
        return new AssetRecommendation(
                new Isin(isin),
                RecommendationType.valueOf(recommendationType));
    }

    public String isin() {
        return isin;
    }

    public AssetRecommendationJpaEntity setIsin(String isin) {
        this.isin = isin;
        return this;
    }

    public String recommendationType() {
        return recommendationType;
    }

    public AssetRecommendationJpaEntity setRecommendationType(String recommendationType) {
        this.recommendationType = recommendationType;
        return this;
    }
}
