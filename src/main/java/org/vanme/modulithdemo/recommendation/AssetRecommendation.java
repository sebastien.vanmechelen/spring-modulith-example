package org.vanme.modulithdemo.recommendation;

import org.jmolecules.ddd.annotation.AggregateRoot;
import org.jmolecules.ddd.annotation.Identity;
import org.vanme.modulithdemo.asset.Isin;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;

@AggregateRoot
public class AssetRecommendation {

    @Identity
    private final Isin isin;
    private final List<Object> events;
    private RecommendationType recommendationType;

    public AssetRecommendation(Isin isin) {
        this(isin, RecommendationType.NONE);
    }

    public AssetRecommendation(Isin isin, RecommendationType recommendationType) {
        this.isin = requireNonNull(isin);
        this.recommendationType = requireNonNull(recommendationType);
        this.events = new ArrayList<>();
    }

    public Isin getIsin() {
        return isin;
    }

    public RecommendationType getRecommendationType() {
        return recommendationType;
    }

    public List<Object> getEvents() {
        return List.copyOf(events);
    }

    public AssetRecommendation changeRecommendation(RecommendationType newRecommendationType) {
        if (recommendationType != newRecommendationType) {
            events.add(new AssetRecommendationChangedEvent(isin, recommendationType, newRecommendationType));
            recommendationType = newRecommendationType;
        }
        return this;
    }
}
