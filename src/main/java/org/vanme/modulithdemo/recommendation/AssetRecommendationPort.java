package org.vanme.modulithdemo.recommendation;

import org.jmolecules.architecture.hexagonal.SecondaryPort;
import org.vanme.modulithdemo.asset.Isin;

import java.util.Collection;

@SecondaryPort
public interface AssetRecommendationPort {

    Collection<AssetRecommendation> findAll();

    AssetRecommendation getByIsin(Isin isin);

    void save(AssetRecommendation assetRecommendation);

}
