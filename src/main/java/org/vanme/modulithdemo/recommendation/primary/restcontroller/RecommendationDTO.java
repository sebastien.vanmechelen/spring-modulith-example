package org.vanme.modulithdemo.recommendation.primary.restcontroller;

import org.vanme.modulithdemo.recommendation.AssetRecommendation;

class RecommendationDTO {

    private final String isin;
    private final String recommendationType;

    public RecommendationDTO(AssetRecommendation assetRecommendation) {
        this.isin = assetRecommendation.getIsin().value();
        this.recommendationType = assetRecommendation.getRecommendationType().name();
    }

    public String getIsin() {
        return isin;
    }

    public String getRecommendationType() {
        return recommendationType;
    }
}
