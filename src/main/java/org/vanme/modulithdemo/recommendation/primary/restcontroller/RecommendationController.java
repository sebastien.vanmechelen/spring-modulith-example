package org.vanme.modulithdemo.recommendation.primary.restcontroller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.vanme.modulithdemo.asset.Isin;
import org.vanme.modulithdemo.recommendation.AssetRecommendationPort;
import org.vanme.modulithdemo.recommendation.CreateOrUpdateAssetRecommendationUseCase;
import org.vanme.modulithdemo.recommendation.RecommendationType;

import java.util.List;

@RestController
@RequestMapping(path = "/recommendations")
class RecommendationController {

    private final AssetRecommendationPort assetRecommendationPort;
    private final CreateOrUpdateAssetRecommendationUseCase createOrUpdateAssetRecommendationUseCase;

    public RecommendationController(AssetRecommendationPort assetRecommendationPort,
                                    CreateOrUpdateAssetRecommendationUseCase createOrUpdateAssetRecommendationUseCase) {

        this.assetRecommendationPort = assetRecommendationPort;
        this.createOrUpdateAssetRecommendationUseCase = createOrUpdateAssetRecommendationUseCase;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<RecommendationDTO> getAllAssetRecommendations() {
        return assetRecommendationPort.findAll()
                .stream()
                .map(RecommendationDTO::new)
                .toList();
    }

    @PostMapping(
            path = "/{isin}/{recommendationType}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Void> createOrUpdateRecommendation(@PathVariable("isin") String isin,
                                                      @PathVariable("recommendationType") String recommendationType) {

        createOrUpdateAssetRecommendationUseCase.execute(new Isin(isin), RecommendationType.valueOf(recommendationType));
        // TODO - Return 201 if new and 200 if updated ?
        return ResponseEntity.ok().build();
    }

}
