package org.vanme.modulithdemo.recommendation.internal;

import org.jmolecules.architecture.hexagonal.PrimaryAdapter;
import org.springframework.stereotype.Service;
import org.vanme.modulithdemo.asset.Isin;
import org.vanme.modulithdemo.recommendation.AssetRecommendationPort;
import org.vanme.modulithdemo.recommendation.CreateOrUpdateAssetRecommendationUseCase;
import org.vanme.modulithdemo.recommendation.RecommendationType;

@Service
@PrimaryAdapter
class CreateOrUpdateAssetRecommendationService implements CreateOrUpdateAssetRecommendationUseCase {

    private final AssetRecommendationPort assetRecommendationPort;

    public CreateOrUpdateAssetRecommendationService(AssetRecommendationPort assetRecommendationPort) {
        this.assetRecommendationPort = assetRecommendationPort;
    }

    @Override
    public void execute(Isin isin, RecommendationType recommendationType) {
        var assetRecommendation = assetRecommendationPort
                .getByIsin(isin)
                .changeRecommendation(recommendationType);

        assetRecommendationPort.save(assetRecommendation);
    }
}
