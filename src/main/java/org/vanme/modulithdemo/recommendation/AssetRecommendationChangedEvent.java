package org.vanme.modulithdemo.recommendation;

import org.jmolecules.event.annotation.DomainEvent;
import org.vanme.modulithdemo.asset.Isin;

@DomainEvent
public record AssetRecommendationChangedEvent(Isin isin, RecommendationType before, RecommendationType after) {

    public AssetRecommendationChangedEvent {
        if (before == after) {
            throw new IllegalArgumentException("Cannot build event if recommendation does not changed from '%s'".formatted(before));
        }
    }

    public AssetRecommendationChangedEvent(Isin isin, RecommendationType after) {
        this(isin, RecommendationType.NONE, after);
    }

    public boolean isSellingRecommendation() {
        return after == RecommendationType.SELL;
    }

    public boolean isBuyingRecommendation() {
        return after == RecommendationType.BUY;
    }

}
