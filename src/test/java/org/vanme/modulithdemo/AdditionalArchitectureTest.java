package org.vanme.modulithdemo;

import com.tngtech.archunit.core.domain.JavaModifier;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.jmolecules.archunit.JMoleculesArchitectureRules;
import org.jmolecules.ddd.annotation.Repository;
import org.jmolecules.ddd.annotation.ValueObject;
import org.springframework.web.bind.annotation.RestController;

import static com.tngtech.archunit.lang.conditions.ArchConditions.haveOnlyFinalFields;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices;

@AnalyzeClasses(packages = "org.vanme.modulithdemo", importOptions = {
        ImportOption.DoNotIncludeTests.class
})
class AdditionalArchitectureTest {

    // Useless with Spring Modulith, just for demo purpose
    @ArchTest
    ArchRule featuresShouldNotHaveCyclicDependencies = slices()
            .matching("org.vanme.modulithdemo.(*)..")
            .should()
            .beFreeOfCycles();

    @ArchTest
    ArchRule ensureHexagonalArchitecture = JMoleculesArchitectureRules.ensureHexagonal();

    // Rules not implemented in JMolecules yet
    @ArchTest
    ArchRule repositoriesShouldResideInSecondaryPackages = classes()
            .that()
            .areAnnotatedWith(Repository.class)
            .should()
            .resideInAPackage("..secondary.persistence..")
            .because("persistence details should part of our secondary adapters");

    @ArchTest
    ArchRule valueObjectsShouldBeImmutable = classes()
            .that()
            .areAnnotatedWith(ValueObject.class)
            .should()
            .haveModifier(JavaModifier.FINAL)
            .andShould(haveOnlyFinalFields())
            .because("value objects should be immutable");

    @ArchTest
    ArchRule restControllersShouldResideInPrimaryPackages = classes()
            .that()
            .areAnnotatedWith(RestController.class)
            .should()
            .resideInAPackage("..primary.restcontroller..")
            .because("rest controllers should part of our primary adapters");

}
