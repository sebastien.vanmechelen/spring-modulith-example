package org.vanme.modulithdemo.recommendation;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.modulith.test.ApplicationModuleTest;
import org.springframework.modulith.test.AssertablePublishedEvents;
import org.vanme.modulithdemo.asset.Isin;
import org.vanme.modulithdemo.recommendation.secondary.persistence.AssetRecommendationJpaEntity;
import org.vanme.modulithdemo.recommendation.secondary.persistence.AssetRecommendationJpaRepository;

import static org.assertj.core.api.Assertions.assertThat;

@ApplicationModuleTest(verifyAutomatically = false)
class RecommendationModuleIntegrationTest {

    private static final Isin FR0011716265 = new Isin("FR0011716265");

    // Can be tested by directly triggering the controller if I am making it public
    @Autowired
    private CreateOrUpdateAssetRecommendationUseCase createOrUpdateAssetRecommendationUseCase;

    @Autowired
    private AssetRecommendationJpaRepository assetRecommendationJpaRepository;

    @AfterEach
    void tearDown() {
        assetRecommendationJpaRepository.deleteAll();
    }

    @Test
    void creatingBuyRecommendation_shouldTriggerRecommendationChangeEvent(AssertablePublishedEvents events) {
        createOrUpdateAssetRecommendationUseCase.execute(FR0011716265, RecommendationType.BUY);

        assertThat(events)
                .contains(AssetRecommendationChangedEvent.class)
                .matching(AssetRecommendationChangedEvent::before, RecommendationType.NONE)
                .matching(AssetRecommendationChangedEvent::after, RecommendationType.BUY)
                .matching(AssetRecommendationChangedEvent::isin, FR0011716265);
    }

    @Test
    void creatingSellRecommendation_shouldTriggerRecommendationChangeEvent(AssertablePublishedEvents events) {
        createOrUpdateAssetRecommendationUseCase.execute(FR0011716265, RecommendationType.SELL);

        assertThat(events)
                .contains(AssetRecommendationChangedEvent.class)
                .matching(AssetRecommendationChangedEvent::before, RecommendationType.NONE)
                .matching(AssetRecommendationChangedEvent::after, RecommendationType.SELL)
                .matching(AssetRecommendationChangedEvent::isin, FR0011716265);
    }

    @Test
    void creatingAlreadyExistingRecommendation_shouldNotTriggerRecommendationChangeEvent(AssertablePublishedEvents events) {
        setUpExistingRecommendation(RecommendationType.BUY);

        createOrUpdateAssetRecommendationUseCase.execute(FR0011716265, RecommendationType.BUY);

        assertThat(events.ofType(AssetRecommendationChangedEvent.class)).isEmpty();
    }

    @Test
    void updatingRecommendationFromSellToBuy_shouldTriggerRecommendationChangeEvent(AssertablePublishedEvents events) {
        setUpExistingRecommendation(RecommendationType.SELL);

        createOrUpdateAssetRecommendationUseCase.execute(FR0011716265, RecommendationType.BUY);

        assertThat(events)
                .contains(AssetRecommendationChangedEvent.class)
                .matching(AssetRecommendationChangedEvent::before, RecommendationType.SELL)
                .matching(AssetRecommendationChangedEvent::after, RecommendationType.BUY)
                .matching(AssetRecommendationChangedEvent::isin, FR0011716265);
    }

    @Test
    void updatingRecommendationFromBuyToSell_shouldTriggerRecommendationChangeEvent(AssertablePublishedEvents events) {
        setUpExistingRecommendation(RecommendationType.BUY);

        createOrUpdateAssetRecommendationUseCase.execute(FR0011716265, RecommendationType.SELL);

        assertThat(events)
                .contains(AssetRecommendationChangedEvent.class)
                .matching(AssetRecommendationChangedEvent::before, RecommendationType.BUY)
                .matching(AssetRecommendationChangedEvent::after, RecommendationType.SELL)
                .matching(AssetRecommendationChangedEvent::isin, FR0011716265);
    }

    private void setUpExistingRecommendation(RecommendationType recommendationType) {
        assetRecommendationJpaRepository.save(
                new AssetRecommendationJpaEntity()
                        .setIsin(FR0011716265.value())
                        .setRecommendationType(recommendationType.name()));
    }
}
