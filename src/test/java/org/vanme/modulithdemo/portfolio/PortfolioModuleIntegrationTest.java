package org.vanme.modulithdemo.portfolio;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.modulith.test.ApplicationModuleTest;
import org.springframework.modulith.test.Scenario;
import org.springframework.transaction.support.TransactionTemplate;
import org.vanme.modulithdemo.asset.Isin;
import org.vanme.modulithdemo.portfolio.secondary.persistence.PortfolioJpaRepository;
import org.vanme.modulithdemo.recommendation.AssetRecommendationChangedEvent;
import org.vanme.modulithdemo.recommendation.RecommendationType;

import java.math.BigDecimal;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;

@ApplicationModuleTest(verifyAutomatically = false)
class PortfolioModuleIntegrationTest {

    private static final PortfolioId PORTFOLIO_ID = new PortfolioId("7b67169b-2f83-40ed-af09-fb81442787a8");

    @Autowired
    private TransactionTemplate txTemplate;

    @SpyBean
    private PortfolioJpaRepository portfolioJpaRepository;

    // TODO - Side effect to fix because tests will modify the portfolio state into DB

    @Test
    void whenSellingRecommendationIsPublished_thenPositionIsSoldOnPortfolio(Scenario scenario) {
        var isin = new Isin("FR0011716265");

        assertThatPortfolio(portfolio -> assertThat(portfolio.getQuantity(isin))
                        .isEqualByComparingTo(BigDecimal.valueOf(1000)));

        scenario.publish(new AssetRecommendationChangedEvent(isin, RecommendationType.SELL))
                .andWaitForEventOfType(PositionChangedEvent.class)
                .toArriveAndVerify(event -> {
                    assertThat(event.portfolioId()).isEqualTo(PORTFOLIO_ID);
                    assertThat(event.isin()).isEqualTo(isin);
                    assertThat(event.variation()).isEqualByComparingTo(BigDecimal.valueOf(-1000));

                    assertThatPortfolio(portfolio -> assertThat(portfolio.noPositionOn(isin)).isTrue());
                });
    }

    @Test
    void whenBuyingRecommendationIsPublished_thenPositionIsCreatedOnPortfolio(Scenario scenario) {
        var isin = new Isin("GB00BN7CG237");

        assertThatPortfolio(portfolio ->
                assertThat(portfolio.noPositionOn(isin)).isTrue());

        scenario.publish(new AssetRecommendationChangedEvent(isin, RecommendationType.BUY))
                .andWaitForEventOfType(PositionChangedEvent.class)
                .toArriveAndVerify(event -> {
                    assertThat(event.portfolioId()).isEqualTo(PORTFOLIO_ID);
                    assertThat(event.isin()).isEqualTo(isin);
                    assertThat(event.variation()).isEqualByComparingTo(BigDecimal.valueOf(10));

                    assertThatPortfolio(portfolio -> assertThat(portfolio.hasPositionOn(isin)).isTrue());
                });
    }

    @Test
    void whenBuyingRecommendationIsPublished_thenPositionIsIncreasedByTenOnPortfolio(Scenario scenario) {
        var isin = new Isin("NL0000235190");

        assertThatPortfolio(portfolio -> assertThat(portfolio.getQuantity(isin))
                .isEqualByComparingTo(BigDecimal.valueOf(200)));

        scenario.publish(new AssetRecommendationChangedEvent(isin, RecommendationType.BUY))
                .andWaitForEventOfType(PositionChangedEvent.class)
                .toArriveAndVerify(event -> {
                    assertThat(event.portfolioId()).isEqualTo(PORTFOLIO_ID);
                    assertThat(event.isin()).isEqualTo(isin);
                    assertThat(event.variation()).isEqualByComparingTo(BigDecimal.valueOf(10));

                    assertThatPortfolio(portfolio -> assertThat(portfolio.getQuantity(isin))
                            .isEqualByComparingTo(BigDecimal.valueOf(210)));
                });
    }

    private void assertThatPortfolio(Consumer<Portfolio> consumer) {
        var portfolio = txTemplate.execute((txStatus) -> portfolioJpaRepository
                .getReferenceById(PORTFOLIO_ID.value())
                .toPortfolio());

        assertThat(portfolio).satisfies(consumer);
    }
}
