package org.vanme.modulithdemo;

import org.junit.jupiter.api.Test;
import org.springframework.modulith.core.ApplicationModules;
import org.springframework.modulith.docs.Documenter;

class ModularityTests {

    private final ApplicationModules modules = ApplicationModules.of(ModulithDemoApplication.class);

    @Test
    void writeDocumentationSnippets() {
        var canvasOptions = Documenter.CanvasOptions.defaults();

        var docOptions = Documenter.DiagramOptions.defaults()
                .withStyle(Documenter.DiagramOptions.DiagramStyle.UML);

        new Documenter(modules)
                .writeDocumentation(docOptions, canvasOptions);
    }

    @Test
    void checkDependencies() {
        modules.verify();
    }
}
