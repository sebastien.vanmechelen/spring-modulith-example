# Spring Modulith Sample Project

This project is used as support of my presentation available on [Gitlab](https://sebastien.vanmechelen.gitlab.io/spring-modulith-presentation).

The purpose is to assess and highlight capabilities of:
- [ArchUnit](https://www.archunit.org/)
- [jMolecule](https://github.com/xmolecules/jmolecules)
- [Spring Modulith](https://spring.io/blog/2022/10/21/introducing-spring-modulith)

## Experimentation
### Breaking the architecture
- Module Dependencies
  - [ ] Cyclic dependencies between modules
    - In CreateOrUpdateAssetRecommendationService, add PortfolioPort to process recommendation on portfolio
  - [ ] Dependencies to internal package of another module
    - Create a dummy pojo in org.vanme.modulithdemo.recommendation.internal
    - Instantiate it in a class of portfolio module
- Hexagonal Architecture
  - [ ] Domain depends upon driver/driven
    - In CreateOrUpdateAssetRecommendationService, add AssetRecommendationJpaRepository repository
- Domain Driven Design
  - [ ] Aggregate root without ID
    - Removing Identity annotation on portfolio ID

## Documentation
Let's have a look at the `index.adoc` file!